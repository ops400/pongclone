#include <raylib.h>
#include <stdio.h>
#define WHEIGHT 160
#define WWIDTH 240
#define WTITLE "Pong Clone"

/*NOTE: the player and ball collision bug*/

struct playerGeneral{
    int h, w;//height and width
    float x,y;//position
    int points;
    char pc;//points Char
};

struct ball{
    float r;//raidus
    int vx, vy;
    float x, y, cl, cr, cb, cu;
};

int mv = 2;//movement velocity
char convert(char a[1], int b);
char* p2pcDebug;

int main(int argc,char** argv){
    struct playerGeneral p1;
    struct playerGeneral p2;
    struct ball b;
    //player 1 section
    p1.h = 40;
    p1.w = 5;
    p1.x = 0;
    p1.y = (WHEIGHT-p1.h)/2;
    p1.points = 0;
    Rectangle p1Rec = {p1.x,p1.y,p1.w,p1.h};
    //player 2 section
    p2.h = p1.h;
    p2.w = p1.w;
    p2.x = WWIDTH-p2.w;
    p2.y = (WHEIGHT-p1.h)/2;
    p2.points = 0;
    Rectangle p2Rec = {p2.x,p2.y,p2.w,p2.h};
    //ball section
    b.r = 2.5;
    b.x = (WWIDTH-b.r)/2;
    b.y = (WHEIGHT-b.r)/2;
    b.vx = -5;
    b.vy = -5;
    Vector2 ballAxis;
    InitWindow(WWIDTH, WHEIGHT, WTITLE);
    SetTargetFPS(30);
    while(!WindowShouldClose()){
        //player 1 movement logic
        if(IsKeyDown(KEY_UP) && p1.y >= 0) p1.y -= mv;
        if(IsKeyDown(KEY_DOWN) && p1.y <= WHEIGHT-p1.h) p1.y += mv;
        Rectangle p1Rec = {p1.x,p1.y,p1.w,p1.h};
        //player 2 movement logic
        if(IsKeyDown(KEY_W) && p2.y >= 0) p2.y -= mv;
        if(IsKeyDown(KEY_S)&& p2.y <= WHEIGHT-p2.h) p2.y += mv;
        Rectangle p2Rec = {p2.x,p2.y,p2.w,p2.h};
        //ball corners
        b.cl = b.x - b.r;
        b.cr = b.x + b.r;
        b.cu = b.y - b.r;
        b.cb = b.y + b.r;
        //debug info
        printf("b.vx=%i\nb.vy=%i\n",b.vx,b.vy);
        printf("b.x=%f\nb.y=%f\n",b.x,b.y);
        printf("b.cl=%f\nb.cr=%f\nb.cu=%f\nb.cb=%f\n",b.cl,b.cr,b.cu,b.cb);
        //ballAxis value transfer
        ballAxis.x = b.x;
        ballAxis.y = b.y;
        //ball colision logic
        if(b.cu <= 0 ||
        b.cb >= WHEIGHT ||
        CheckCollisionCircleRec(ballAxis, b.r, p1Rec) == true ||
        CheckCollisionCircleRec(ballAxis, b.r, p2Rec) == true){
            b.vy = -b.vy;
            printf("collision y axis\n");
        }
        if(b.cl <= 0 ||
        b.cr >= WWIDTH ||
        CheckCollisionCircleRec(ballAxis, b.r, p1Rec) == true ||
        CheckCollisionCircleRec(ballAxis, b.r, p2Rec) == true){
            b.vx = -b.vx;
            printf("collision x axis\n");
        }
        //ball movement logic
        b.x = b.x+b.vx;
        b.y = b.y+b.vy;
        //score system
        if(b.cl <= 0) p2.points += 1;
        if(b.cr >= WWIDTH) p1.points += 1;
        //more debug info
        printf("p1.points=%i\np2.poinst=%i\n",p1.points,p2.points);
        printf("p2.points_mod1=%i\n",(p2.points/1));
        printf("p2.points_mod10=%i\n",(p2.points/10));
        printf("p2.points_mod100=%i\n",(p2.points/100));
        //int to char conversion for p1.points and p2.points
        p1.pc = convert(&p1.pc, p1.points);
        p2.pc = convert(&p2.pc, p2.points);
        //even more debug info
        p2pcDebug = &p2.pc;
        printf("p2.pc=%s\n",p2pcDebug);
        //render thingy
        BeginDrawing();
        DrawLine(WWIDTH/2, 0, WWIDTH/2, WHEIGHT, RAYWHITE);
        if(p1.points <= 9) DrawText(&p1.pc, (WWIDTH/2)-11, 2, 10, RAYWHITE);
        if(p1.points > 9 && p1.points <= 99) DrawText(&p1.pc, (WWIDTH/2)-15, 2, 10, RAYWHITE);
        if(p1.points > 99 && p1.points <= 999) DrawText(&p1.pc, (WWIDTH/2)-20, 2, 10, RAYWHITE);
        DrawText(&p2.pc, (WWIDTH/2)+4, 2, 10, RAYWHITE);
        DrawRectangleRec(p1Rec, RAYWHITE);//draws player 1
        DrawRectangleRec(p2Rec, RAYWHITE);//draws player 2
        DrawCircle(b.x, b.y, b.r, RAYWHITE);//draws the ball
        ClearBackground(BLACK);
        EndDrawing();
    }

    return 0;
}

char convert(char a[1], int b){
    for(int i = 0;i <= 1; i++){
        a[i] = '\0';
    }
    if(b <= 9){
        a[0] = b + '0';
    }
    if(b > 9 && b <= 99){
        a[0] = (b/10) + '0';
        a[1] = (b-((b/10)*10)) + '0';
    }
    if(b > 99 && b <= 999){
        a[0] = (b/100) + '0';
        a[1] = ((b/10)-((b/100)*10)) + '0';
        a[2] = (b-((b/10)*10)) + '0';
    }
    return *a;
}